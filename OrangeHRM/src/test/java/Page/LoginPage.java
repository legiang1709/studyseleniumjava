package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import testpackage.TestBase;

public class LoginPage extends TestBase {
    private String orangeHRM_Url = "https://opensource-demo.orangehrmlive.com/";
    private static By userNameLocator = By.name("username");
    private static By passwordLocator = By.name("password");
    private static By loginButtonLocator = By.cssSelector("button[type='submit']");

    public static void loginAccount(String username, String password) {
        WebElement userNameField = driver.findElement(userNameLocator);
        WebElement passwordField = driver.findElement(passwordLocator);
        WebElement loginButton = driver.findElement(loginButtonLocator);
        userNameField.sendKeys(username);
        passwordField.sendKeys(password);
        loginButton.click();
    }
}
