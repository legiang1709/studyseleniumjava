package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import testpackage.TestBase;

public class AdminPage extends TestBase {
    private static By adminMenuLocator = By.linkText("Admin");
    public static void navigateToAdminPage() {
        WebElement adminMenu = driver.findElement(adminMenuLocator);
        adminMenu.click();
    }
}
