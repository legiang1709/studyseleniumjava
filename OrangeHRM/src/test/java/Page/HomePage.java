package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import testpackage.TestBase;

public class HomePage extends TestBase {
    private static By accountNameLabelLocator = By.className("oxd-userdropdown-name");
    public static void printAccountName(){
        WebElement accountNameLabel = driver.findElement(accountNameLabelLocator);
        System.out.println(accountNameLabel.getText());
    }
}
