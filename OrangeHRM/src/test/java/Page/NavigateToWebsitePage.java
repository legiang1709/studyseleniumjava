package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import testpackage.TestBase;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class NavigateToWebsitePage extends TestBase {
    private static String url = "https://opensource-demo.orangehrmlive.com/";
    private static By loginButtonLocator = By.cssSelector("button[type='submit']");
    public static void openBrowser() {
        driver = new ChromeDriver(setDesiredCapabilities());
    }
    public static void navigateToWebsite(String url) {
        driver.get(url);
        driver.manage().window().maximize();
//        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        explicitWaitForVisible(5,loginButtonLocator);
    }

    public static void openBrowserAndNavigateToOrangeHRM() {
        openBrowser();
        navigateToWebsite(url);
    }
}
