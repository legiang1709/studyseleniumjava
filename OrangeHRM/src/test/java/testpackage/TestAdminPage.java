package testpackage;

import Page.AdminPage;
import Page.HomePage;
import Page.LoginPage;
import Page.NavigateToWebsitePage;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TestAdminPage extends TestBase{
    NavigateToWebsitePage navigateToWebsitePage;
    LoginPage loginPage;
    HomePage homePage;
    AdminPage adminPage;
    By adminLabelLocator = By.xpath("//h6[text()='Admin']");
    String expected_AdminUrl = "https://opensource-demo.orangehrmlive.com/web/index.php/admin/viewSystemUsers";
    @BeforeTest
    public void beforeTest(){
        super.beforeTest();
    }
    @AfterTest
    public void afterTest() {
        super.afterTest();
    }
    @Test
    @Parameters({"username","password"})
    public void testNavigateAdminPage(String username, String password) {
        loginPage = new LoginPage();
        loginPage.loginAccount(username, password);
        adminPage = new AdminPage();
        adminPage.navigateToAdminPage();
        String actual_AdminUrl = adminPage.getCurrentUrl();
        Assert.assertEquals(actual_AdminUrl,expected_AdminUrl);
        Assert.assertEquals(driver.findElement(adminLabelLocator).isDisplayed(), true);
    }


}
