package testpackage;

import Page.LoginPage;
import Page.NavigateToWebsitePage;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.Properties;

public class TestLoginPage extends TestBase {
    NavigateToWebsitePage navigateToWebsitePage;
    LoginPage loginPage;
    private static By dashBoardLabelLocator = By.tagName("h6");
    private static By accountNameLabelLocator = By.className("oxd-userdropdown-name");
    @BeforeTest
    public void beforeTest(){
        super.beforeTest();
    }

    @AfterTest
    public void afterTest() {
        super.afterTest();
    }
//    @Test(dataProvider = "adminAccount")
    @Test
    public void testLoginAdminSuccess() throws IOException {
        Properties obj = loadPropertiesFile();
        loginPage = new LoginPage();
        loginPage.loginAccount(obj.getProperty("username_Admin"), obj.getProperty("password_Admin"));
        Assert.assertEquals(driver.findElement(dashBoardLabelLocator).isDisplayed(), true);
        Assert.assertEquals(driver.findElement(accountNameLabelLocator).isDisplayed(), true);
    }
}
