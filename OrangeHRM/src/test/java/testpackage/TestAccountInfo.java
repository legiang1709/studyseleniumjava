package testpackage;

import Page.HomePage;
import Page.LoginPage;
import Page.NavigateToWebsitePage;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TestAccountInfo extends TestBase {
    NavigateToWebsitePage navigateToWebsitePage;
    LoginPage loginPage;
    HomePage homePage;
    @BeforeTest
    public void beforeTest(){
        super.beforeTest();
    }
    @AfterTest
    public void afterTest() {
        super.afterTest();
    }
    @Test(dataProvider = "adminAccount")
    public void testAccountInformation(String username, String password) {
        loginPage = new LoginPage();
        loginPage.loginAccount(username, password);
        homePage = new HomePage();
        homePage.printAccountName();
    }

}
