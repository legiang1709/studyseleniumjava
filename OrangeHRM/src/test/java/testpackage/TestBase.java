package testpackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TestBase {
    public static WebDriver driver;
    private String orangeHRM_url = "https://opensource-demo.orangehrmlive.com/";
    public void afterTest() {
        driver.close();
        driver.quit();
    }
    public void beforeTest(){
        driver = new ChromeDriver(setDesiredCapabilities());
        driver.get(orangeHRM_url);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }
    @DataProvider(name = "adminAccount")
    //@DataProvider(name = "adminAccount", parallel = true) --> using parallel = true to run parallel datasets
    public Object[][] dataAcount() {
        return new Object[][]{{"Admin", "admin123"}};
    }
    public static ChromeOptions setDesiredCapabilities(){
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        options.merge(capabilities);
        return options;
    }
    public static Properties loadPropertiesFile() throws IOException {
        Properties obj = new Properties();
        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir")+"\\src\\main\\java\\resources\\configs.properties");
        obj.load(objfile);
        return obj;
    }
    public static void enterText(By locator, String text) {
        driver.findElement(locator).sendKeys(text);
    }

    public static void click(By locator) {
        driver.findElement(locator).click();
    }

    public static void checkVisibility(By locator) {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        Assert.assertEquals(driver.findElement(locator).isDisplayed(), true);
    }

    public static void checkEmpty(By locator) {
        Assert.assertNotNull(driver.findElement(locator));
    }

    public static void implicitWait(int seconds) {
        driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
    }
    public static void explicitWaitForVisible(int seconds, By locator){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(seconds));
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    public static String getTextFromElement(By locator) {
        WebElement element = driver.findElement(locator);
        return element.getText();
    }

}
