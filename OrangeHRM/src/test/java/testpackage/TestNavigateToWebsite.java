package testpackage;
import Page.NavigateToWebsitePage;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
public class TestNavigateToWebsite extends TestBase {

    NavigateToWebsitePage navigateToWebsite;
    String expectedUrl = "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login";
    By loginButtonLocator = By.xpath("//button[@type='submit']");

    @AfterTest
    public void afterTest(){
        super.afterTest();
    }
    @Test
    public void testNavigateToOrangeHRM() {
        navigateToWebsite = new NavigateToWebsitePage();
        navigateToWebsite.openBrowserAndNavigateToOrangeHRM();
        String actualUrl = navigateToWebsite.getCurrentUrl();
        Assert.assertEquals(actualUrl,expectedUrl);
        Assert.assertEquals(driver.findElement(loginButtonLocator).isDisplayed(), true);
    }
}
